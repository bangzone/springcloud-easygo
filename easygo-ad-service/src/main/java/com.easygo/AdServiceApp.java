package com.easygo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;


/**
 * @author luke’s
 */
@SpringBootApplication
@EnableEurekaClient
public class AdServiceApp {

    public static void main(String[] args) {
        System.out.println("广告服务9004.......");
        SpringApplication.run(AdServiceApp.class,args);
    }
}
