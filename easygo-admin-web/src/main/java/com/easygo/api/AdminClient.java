package com.easygo.api;

import com.easygo.pojo.Admin;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;


@FeignClient(value = "easygo-admin-service")
public interface AdminClient {

    @RequestMapping("/admin_login")
    public Admin adminLogin(@RequestBody Admin admin);

}
