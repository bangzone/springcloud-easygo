package com.easygo.api;

import com.easygo.pojo.Specification;
import com.easygo.utils.PageUtils;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


@FeignClient(value = "easygo-specification-service")
public interface SpecificationClient {

    @RequestMapping("/specification_add")
    public Integer specification_add(@RequestParam("spec_name") String spec_name, @RequestParam("option_name") String[] option_name, @RequestParam("orders") Integer[] orders);

        /**
         * 规格服务的后台
         * @param pageIndex
         * @param pageSize
         * @param spec_name
         * @return
         */
    @RequestMapping("/specification_pages")
    public PageUtils<Specification> specification_pages(
            @RequestParam(defaultValue = "1", required = false) Integer pageIndex,
            @RequestParam(defaultValue = "5", required = false) Integer pageSize, @RequestParam(defaultValue = "", required = false) String spec_name);

}
