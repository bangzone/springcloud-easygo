package com.easygo.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class ContentCategory implements Serializable {

    private static final long serialVersionUID = 5266300161133421639L;

    private Integer id;
    private String name;
    private Integer del;
}
