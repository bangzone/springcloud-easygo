package com.easygo.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class Specification implements Serializable {

    private static final long serialVersionUID = 8837759180331415520L;

    private Integer id;
    private String spec_name;
    private Integer del;

}
