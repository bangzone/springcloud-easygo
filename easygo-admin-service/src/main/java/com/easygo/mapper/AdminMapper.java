package com.easygo.mapper;

import com.easygo.pojo.Admin;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface AdminMapper {

    public Admin adminlogin(Admin admin);
}
