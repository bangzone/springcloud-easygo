package com.easygo.mapper;

import com.easygo.pojo.LoginLog;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


@Mapper
public interface LoginLogMapper {

    /**
     * 新增登录日志
     * @param log
     * @return
     */
    public int addLoginLog(LoginLog log);

    /*
     * 获取登录人的日志  获取最近10次登录日志
     */
    public List<LoginLog> getMyLoginLogs(String username);

}
