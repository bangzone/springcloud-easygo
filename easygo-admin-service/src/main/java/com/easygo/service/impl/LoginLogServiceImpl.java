package com.easygo.service.impl;

import com.easygo.mapper.LoginLogMapper;
import com.easygo.pojo.LoginLog;
import com.easygo.service.LoginLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service
public class LoginLogServiceImpl implements LoginLogService {

    @Resource
    LoginLogMapper loginLogMapper;

    @Override
    public int addLoginLog(LoginLog log) {
        return loginLogMapper.addLoginLog(log);
    }

    @Override
    public List<LoginLog> getMyLoginLogs(String username) {
        return loginLogMapper.getMyLoginLogs(username);
    }
}
