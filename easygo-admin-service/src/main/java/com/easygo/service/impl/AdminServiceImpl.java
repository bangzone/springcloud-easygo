package com.easygo.service.impl;

import com.easygo.mapper.AdminMapper;
import com.easygo.pojo.Admin;
import com.easygo.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


@Service
public class AdminServiceImpl implements AdminService {

    @Resource
    AdminMapper adminMapper;

    @Override
    public Admin adminlogin(Admin admin) {
        return adminMapper.adminlogin(admin);
    }
}
