package com.easygo.service.impl;

import com.easygo.mapper.SpecificationOptionMapper;
import com.easygo.pojo.SpecificationOption;
import com.easygo.service.SpecificationOptionService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service
public class SpecificationOptionServiceImpl implements SpecificationOptionService {

    @Resource
    SpecificationOptionMapper specificationOptionMapper;

    @Override
    public int addSpecificationOption(SpecificationOption option) {
        return specificationOptionMapper.addSpecificationOption(option);
    }

    @Override
    public List<SpecificationOption> getSpecificationOptionsBySpecificationId(Integer specificationId) {
        return specificationOptionMapper.getSpecificationOptionsBySpecificationId(specificationId);
    }

    @Override
    public int deleteSpecificationOptions(int spec_id) {
        return specificationOptionMapper.deleteSpecificationOptions(spec_id);
    }
}
