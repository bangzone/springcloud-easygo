package com.easygo.service;

import com.easygo.pojo.SpecificationOption;

import java.util.List;


public interface SpecificationOptionService {

    //新增
    public int addSpecificationOption(SpecificationOption option);

    public List<SpecificationOption> getSpecificationOptionsBySpecificationId(Integer specificationId);;

    public int deleteSpecificationOptions(int spec_id);

}
